#terraform {
#  backend "http" {
#  }
#}
terraform {
  backend "s3" {
    bucket = "terraform-eks-gitlab"
    key    = "terraform-eks-gitlab/statefile.tfstate"
    region = "eu-central-1"
  }
}

